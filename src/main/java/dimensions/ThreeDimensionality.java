package dimensions;

public class ThreeDimensionality extends TwoDimensionality {

    private final int z;

    public ThreeDimensionality(int x, int y, int z) {
        super(x, y);
        this.z = z;
    }

    public int getZ() {
        return z;
    }
}
