package dimensions;

public class Coordinates<T extends TwoDimensionality> {

    private final T[] coordinates;

    public Coordinates(T... coordinates) {
        this.coordinates = coordinates;
    }

    public T[] getCoordinates() {
        return coordinates;
    }
}
