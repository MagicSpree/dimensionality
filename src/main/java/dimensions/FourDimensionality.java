package dimensions;

public class FourDimensionality extends ThreeDimensionality {

    private final int t;

    public FourDimensionality(int x, int y, int z, int t) {
        super(x, y, z);
        this.t = t;
    }
}
