package dimensions;

public class TwoDimensionality {

    private final int x, y;

    public TwoDimensionality(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
