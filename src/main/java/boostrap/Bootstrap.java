package boostrap;

import dimensions.Coordinates;
import dimensions.FourDimensionality;
import dimensions.ThreeDimensionality;
import dimensions.TwoDimensionality;

import java.util.Arrays;
import java.util.Objects;


public class Bootstrap {

    private TwoDimensionality twoDimensionality;
    private ThreeDimensionality threeDimensionality;
    private FourDimensionality fourDimensionality;

    public void run() {
        initObjWithXYCoord();
        initObjWithXYZCoord();
    }

    private void initObjWithXYCoord() {
        twoDimensionality = new TwoDimensionality(2, 3);
        threeDimensionality = new ThreeDimensionality(4, 5, 6);
        fourDimensionality = new FourDimensionality(7, 8, 9, 10);
        Coordinates<TwoDimensionality> coordinates = new Coordinates<>(twoDimensionality, threeDimensionality, fourDimensionality);
        showXY(coordinates);
    }

    private void initObjWithXYZCoord() {
        threeDimensionality = new ThreeDimensionality(9, 10, 11);
        fourDimensionality = new FourDimensionality(12, 13, 14, 15);
        Coordinates<ThreeDimensionality> coordinates = new Coordinates<>(threeDimensionality, fourDimensionality);
        showXYZ(coordinates);
    }

    private void showXY(Coordinates<?> c) {
        System.out.println("Координаты X Y:");
        Arrays.stream(c.getCoordinates())
                .filter(Objects::nonNull)
                .map(this::formingAStringWithXY)
                .forEach(this::print);
        print("");
    }

    private String formingAStringWithXY(TwoDimensionality twoDimensionality) {
        return twoDimensionality.getX() + " " + twoDimensionality.getY();
    }

    private void showXYZ(Coordinates<? extends ThreeDimensionality> c) {
        System.out.println("Координаты X Y Z:");
        Arrays.stream(c.getCoordinates())
                .filter(Objects::nonNull)
                .map(this::formingAStringWithXYZ)
                .forEach(this::print);
        print("");
    }

    private String formingAStringWithXYZ(ThreeDimensionality threeDimensionality) {
        return threeDimensionality.getX() + " " + threeDimensionality.getY() + " "
                + threeDimensionality.getZ();
    }

    private void print(String message) {
        System.out.println(message);
    }
}
